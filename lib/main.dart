import 'dart:js';

import 'package:flutter/material.dart';

Column _buildButtonColumn(Color color, IconData icon, String label) {
  return Column(
    mainAxisSize: MainAxisSize.min,
    children: [
      Icon(
        icon,
        color: color,
      ),
      Container(
        child: Text(
          label,
          style: TextStyle(
            fontSize: 12,
            fontWeight: FontWeight.w400,
            color: color,
          ),
        ),
      ),
    ],
  );
}

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Color color = Theme.of(context).primaryColor;
    Widget titleSection = Container(
      padding: const EdgeInsets.all(32),
      child: Row(
        children: [
          Expanded(
              child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                  padding: const EdgeInsets.only(
                    bottom: 8,
                  ),
                  child: Text(
                    'Natchanan Madcha',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  )),
              Text('Looknam', style: TextStyle(color: Colors.grey[500]))
            ],
          )),
          Icon(
            Icons.add_ic_call_rounded,
            color: Colors.pinkAccent,
          ),
          Text('091-534-8439'),
          Icon(
            Icons.attach_email_rounded,
            color: Colors.pinkAccent,
          ),
          Text('mizuna15617@gmail.com'),
        ],
      ),
    );

    Widget buttonSection = Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          _buildButtonColumn(
              Colors.pinkAccent, Icons.female_rounded, 'Gender: female'),
          _buildButtonColumn(Colors.pinkAccent, Icons.cake_rounded,
              'Birthday: 5 September 1999'),
          _buildButtonColumn(
              Colors.pinkAccent, Icons.home, 'Address: 243/117 PrachinBuri'),
          _buildButtonColumn(
              Colors.pinkAccent, Icons.access_time, 'Free Time: Play Game'),
          _buildButtonColumn(
              Colors.pinkAccent, Icons.tv, 'Like: boku no hero academia'),
          _buildButtonColumn(
              Colors.pinkAccent, Icons.border_color_rounded, 'Color: Pink'),
        ],
      ),
    );

    Widget buttonSection2 = Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          _buildButtonColumn(Colors.purple, Icons.done, 'Java'),
          _buildButtonColumn(Colors.indigo, Icons.done, 'HTML'),
          _buildButtonColumn(Colors.blue, Icons.done, 'C++'),
          _buildButtonColumn(Colors.green, Icons.done, 'python'),
          _buildButtonColumn(Colors.yellow, Icons.done, 'MySQL'),
          _buildButtonColumn(Colors.orange, Icons.done, 'mongoDB'),
          _buildButtonColumn(Colors.red, Icons.done, 'BOOTSTRAP'),
        ],
      ),
    );

    Widget buttonSection3 = Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          _buildButtonColumn(Colors.green, Icons.domain_verification_rounded, 'year 1'),
          _buildButtonColumn(Colors.green, Icons.domain_verification_rounded, 'year 2'),
          _buildButtonColumn(Colors.green, Icons.domain_verification_rounded, 'year 3'),
          _buildButtonColumn(Colors.red, Icons.domain_verification_rounded, 'year 4'),
        ],
      ),
    );

    Widget textSection = Container(
      padding: EdgeInsets.all(32),
      child: Text(
        'Info',
        softWrap: true,
      ),
    );

    Widget textSection2 = Container(
      padding: EdgeInsets.all(32),
      child: Text(
        'Skills',
        softWrap: true,
      ),
    );

    Widget textSection3 = Container(
      padding: EdgeInsets.all(32),
      child: Text(
        'Background and Personal Information',
        softWrap: true,
      ),
    );

    Widget textSection4 = Container(
      padding: EdgeInsets.all(32),
      child: Text(
        'I am capable at creating web pages. Always be inventive so that you can suit the wants of your customers.'
        'Address House No.243/117 ThaTum Road, SriMahaPho Sub-area, PrachinBuri Area, 25140',
        softWrap: true,
      ),
    );

    Widget textSection5 = Container(
      padding: EdgeInsets.all(32),
      child: Text(
        'Education',
        softWrap: true,
      ),
    );

    return MaterialApp(
        title: 'My Resume',
        home: Scaffold(
          appBar: AppBar(
            title: const Text('RESUME'),
          ),
          body: Column(
            children: [
              Image.asset('images/me.jpg',
                  width: 300, height: 300, fit: BoxFit.cover),
              titleSection,
              textSection,
              buttonSection,
              textSection2,
              buttonSection2,
              textSection3,
              textSection4,
              textSection5,
              buttonSection3,
            ],
          ),
        ));
  }
}
